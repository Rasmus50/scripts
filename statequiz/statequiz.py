#!/usr/bin/env python3

import sys
import random

def main():

    states = dict([
        ('Alabama', 'Montgomery'),
        ('Alaska', 'Juneau'),
        ('Arizona', 'Pheonix'),
        ('Arkansas', 'Little Rock'),
        ('California', 'Sacremento'),
        ('Colorado', 'Denver',),
        ('Connecticut', 'Hartford'),
        ('Delaware', 'Dover'),
        ('Florida', 'Tallahassee'),
        ('Georgia', 'Atlanta'),
        ('Hawaii', 'Honolulu'),
        ('Idaho', 'Boise'),
        ('Illinois', 'Springfield'),
        ('Indiana', 'Indianapolis'),
        ('Iowa', 'Des Moines'),
        ('Kansas', 'Topeka'),
        ('Kentucky', 'Frankfort'),
        ('Louisiana', 'Baton Rouge'),
        ('Maine', 'Augusta'),
        ('Maryland', 'Annapolis'),
        ('Massachusetts', 'Boston'),
        ('Michigan', 'Lansing'),
        ('Minnesota', 'St. Paul'),
        ('Mississippi', 'Jackson'),
        ('Missouri', 'Jefferson City'),
        ('Montana', 'Helena'),
        ('Nebraska', 'Lincoln'),
        ('Nevada', 'Carson City'),
        ('New Hampshire', 'Concorde'),
        ('New Jersey', 'Trenton'),
        ('New Mexico', 'Santa Fe'),
        ('New York', 'Albany'),
        ('North Carolina', 'Raleigh'),
        ('North Dakota', 'Bismark'),
        ('Ohio', 'Columbus'),
        ('Oklahoma', 'Oklahoma City'),
        ('Oregon', 'Salem'),
        ('Pennsylvania', 'Harrisburg'),
        ('Rhode Island', 'Providence'),
        ('South Carolina', 'Columbia'),
        ('South Dakota', 'Pierre'),
        ('Tennessee', 'Nashville'),
        ('Texas', 'Austin'),
        ('Utah', 'Salt Lake City'),
        ('Vermont', 'Montpelier'),
        ('Virginia', 'Richmond'),
        ('Washington', 'Olympia'),
        ('West Virginia', 'Charleston'),
        ('Wisconsin', 'Madison'),
        ('Wyoming', 'Cheyenne')
    ])

    corcount = 0
    for x in range(0, 50):
        state = random.choice(list(states.keys()))
        match = str(input("What is the capital of " + state + " ? : "))

        if match.capitalize() == states[state].capitalize():
            print("correct")
            corcount += 1
        else:
            print("Incorrect, the capital of ", state, " is ", states[state])

        states.pop(state)

    print("you got ", corcount, " right.")

main()

