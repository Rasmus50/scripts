#!/bin/env python3

import argparse # command line arguments
import datetime # time formatting
import time # get timestamp

today = datetime.datetime.utcnow()

# parse command line argument(s)
parser = argparse.ArgumentParser()
parser.add_argument("-b", "--birthday", help="birthday formatted in ddmmyyyy")
args = parser.parse_args()

# if birthday argument is specified then convert trailing input to time
if args.birthday:
  birthday = time.mktime(datetime.datetime.strptime(args.birthday, "%d%m%Y").timetuple())
else:
# else prompt user for birthday
  birthday = time.mktime(datetime.datetime.strptime(input('Please enter your bithdate (ddmmyyyy) : '), "%d%m%Y").timetuple())

# retrieve current timestamp and subtract the birthday timestamp
ts = time.time()
# calculate the timestamp difference and convert to days. remove floating point
diff = int((ts - birthday) /60/60/24)

print('{0} days spent on earth'.format(diff))

